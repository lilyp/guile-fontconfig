;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (fontconfig globals)
  #:use-module ((fontconfig bindings) #:prefix ffi:)
  #:use-module ((oop goops) #:select (make is-a?))
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (system foreign)
  #:use-module (system foreign-object)
  #:export (fontconfig-version
            *fontconfig* wrap-config unwrap-config))

(define (fontconfig-version)
  (let* ((%version (ffi:fontconfig-get-version))
         (major rest (floor/ %version 10000))
         (minor patch (floor/ rest 100)))
    (format #f "~a.~a.~a" major minor patch)))

(define (%config-finalize! cfg)
  (let ((ptr (unwrap-config cfg)))
    (unless (null-pointer? ptr)
      (ffi:fontconfig-config-destroy ptr))))

(define-foreign-object-type <fc:config>
  %make-config (config-ptr)
  #:finalizer %config-finalize!)

(define (unwrap-config cfg)
  (make-pointer (config-ptr cfg)))

(define (wrap-config ptr)
  (%make-config (pointer-address ptr)))

(define *fontconfig*
  (make-parameter (%make-config 0)
                  (lambda (cfg)
                    (if (is-a? cfg <fc:config>)
                        cfg
                        (error "not a valid configuration" cfg)))))
