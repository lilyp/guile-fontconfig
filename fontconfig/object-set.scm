;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (fontconfig object-set)
  #:use-module ((fontconfig bindings) #:prefix ffi:)
  #:use-module (ice-9 match)
  #:use-module ((oop goops) #:select (make))
  #:use-module (system foreign)
  #:use-module (system foreign-object)

  #:export (make-object-set
            unwrap-object-set
            object-set-add!
            object-set))

(define (object-set-finalize! object-set)
  (ffi:fontconfig-object-set-destroy
   (unwrap-object-set object-set)))

(define-foreign-object-type <fc:object-set>
  %make-object-set (object-set-ptr)
  #:finalizer object-set-finalize!)

(define (wrap-object-set ptr)
  (%make-object-set (pointer-address ptr)))

(define (unwrap-object-set object-set)
  (if object-set
      (make-pointer (object-set-ptr object-set))
      %null-pointer))

(define (make-object-set . attrs)
  (let* ((ptr (ffi:fontconfig-object-set-create))
         (object-set (wrap-object-set ptr)))
    (for-each (lambda (attr) (object-set-add! object-set attr)) attrs)
    object-set))

;; Adds the attribute @var{attr} to the object-set @var{os}.
(define (object-set-add! os attr)
  (unless
      (eq? 1
           (ffi:fontconfig-object-set-add
            (unwrap-object-set os)
            (match (assoc attr ffi:*attributes*)
              ((_ attr _) (string->pointer attr))
              (#f (error "no such attribute:" attr)))))
    (scm-error 'memory-allocation-error
               "object-set-add!" "failed to add attributes to object-set"
               '() #f)))

(define-syntax-rule (object-set attr attr* ...)
  (make-object-set (quote attr) (quote attr*) ...))
