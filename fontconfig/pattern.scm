;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (fontconfig pattern)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module ((fontconfig bindings) #:prefix ffi:)
  #:use-module (fontconfig globals)
  #:use-module (fontconfig char-set)
  #:use-module (fontconfig object-set)
  #:use-module ((oop goops) #:select (make))
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-26)
  #:use-module (system foreign)
  #:use-module (system foreign-object)
  #:export (make-pattern
            copy-pattern pattern-filter
            pattern=? pattern-subset=
            wrap-pattern unwrap-pattern
            pattern-get string->pattern pattern->string pattern->format
            pattern-substitute pattern-substitute!
            font-match render-prepare))

(define (pattern-finalize! pattern)
  (ffi:fontconfig-pattern-destroy
   (unwrap-pattern pattern)))

(define-foreign-object-type <fc:pattern>
  %make-pattern (pattern-ptr)
  #:finalizer pattern-finalize!)

(define (wrap-pattern ptr)
  (%make-pattern (pointer-address ptr)))

(define (unwrap-pattern pattern)
  (make-pointer (pattern-ptr pattern)))

;; Return @code{#t} if @var{pattern1} and @var{pattern2} are equal.
(define (pattern=? pattern1 pattern2)
  (= 1 (ffi:fontconfig-pattern-equal
        (unwrap-pattern pattern1)
        (unwrap-pattern pattern2))))

;; Return a procedure, that compares two patterns, returning @code{#t} if
;; all the attributes listed in @var{object-set} are equal between them.
(define (pattern-subset= object-set)
  (lambda (pattern1 pattern2)
    (= 1 (ffi:fontconfig-pattern-equal-subset
          (unwrap-pattern pattern1)
          (unwrap-pattern pattern2)
          (unwrap-object-set object-set)))))

;; Add the integer @var{value} to the @var{attribute} of @var{pattern}.
(define (pattern-add-integer! pattern attribute value)
  (ffi:fontconfig-pattern-add-integer
   (unwrap-pattern pattern) (string->pointer attribute) value))

;; Add the character set @var{value} to the @var{attribute} of @var{pattern}.
(define (pattern-add-char-set! pattern attribute value)
  (ffi:fontconfig-pattern-add-char-set
   (unwrap-pattern pattern) (string->pointer attribute)
   (unwrap-char-set value)))

;; Add the double @var{value} to the @var{attribute} of @var{pattern}.
(define (pattern-add-double! pattern attribute value)
  (ffi:fontconfig-pattern-add-double
   (unwrap-pattern pattern) (string->pointer attribute) value))

;; Add the string @var{value} to the @var{attribute} of @var{pattern}.
(define (pattern-add-string! pattern attribute value)
  (ffi:fontconfig-pattern-add-string
   (unwrap-pattern pattern) (string->pointer attribute) (string->pointer value)))

;; Add the boolean @var{value} to the @var{attribute} of @var{pattern}.
(define (pattern-add-bool! pattern attribute value)
  (ffi:fontconfig-pattern-add-bool
   (unwrap-pattern pattern) (string->pointer attribute) (if value 1 0)))

;; Remove the @var{id}'th value of @var{attribute} in @var{pattern}.
;; If @var{id} is @code{#f}, delete all values instead.
(define* (pattern-remove! pattern attribute #:optional id)
  (let ((pattern (unwrap-pattern pattern))
        (attr (string->pointer attribute)))
    (if id
        (ffi:fontconfig-pattern-remove pattern attr id)
        (ffi:fontconfig-pattern-delete pattern attr))))

(define (assoc-ref* alist value msg)
  (let ((handle (assoc value alist)))
    (if handle
        (cdr handle)
        (error msg value))))

(define (make-pattern . args)
  (let* ((ptr (ffi:fontconfig-pattern-create))
         (pattern (if (null-pointer? ptr)
                      (scm-error 'memory-allocation-error
                                 "make-pattern" "failed to create pattern" '() #f)
                      (wrap-pattern ptr))))
    (let loop ((args args))
      (match args
        (() pattern)
        (((? keyword? kwd) value . rest)
         (match (assoc (keyword->symbol kwd) ffi:*attributes*)
           ((_ attr 'fc-string) (pattern-add-string! pattern attr value))
           ((_ attr 'fc-bool) (pattern-add-bool! pattern attr value))
           ((_ attr 'fc-char-set)
            (pattern-add-char-set! pattern attr
                                   (char-set->fc-char-set value)))
           ((_ attr 'fc-double) (pattern-add-double! pattern attr value))
           (('hint-style attr 'fc-integer)
            (pattern-add-integer!
             pattern attr
             (match value
               ((? number? value) value)
               ((? symbol? value)
                (assoc-ref* ffi:fc-hintings value "no such hinting")))))
           (('lcd-filter attr 'fc-integer)
            (pattern-add-integer!
             pattern attr
             (match value
               ((? number? value) value)
               ((? symbol? value)
                (assoc-ref* ffi:fc-lcd-filters value "no such lcd-filter")))))
           (('slant attr 'fc-integer)
            (pattern-add-integer!
             pattern attr
             (match value
               ((? number? value) value)
               ((? symbol? value)
                (assoc-ref* ffi:fc-slants value "no such slant")))))
           (('spacing attr 'fc-integer)
            (pattern-add-integer!
             pattern attr
             (match value
               ((? number? value) value)
               ((? symbol? value)
                (assoc-ref* ffi:fc-spacings value "no such spacing")))))
           (('weight attr 'fc-integer)
            (pattern-add-integer!
             pattern attr
             (match value
               ((? number? value) value)
               ((? symbol? value)
                (assoc-ref* ffi:fc-weights value "no such weight")))))
           (('width attr 'fc-integer)
            (pattern-add-integer!
             pattern attr
             (match value
               ((? number? value) value)
               ((? symbol? value)
                (assoc-ref* ffi:fc-widths value "no such width")))))
           ((_ attr 'fc-integer) (pattern-add-integer! pattern attr value))
           ((_ attr type) (error "no handler for type" type))
           (#f (error "invalid attribute" kwd)))
         (loop rest))))))

;; Create a copy of @var{pattern}, whose memory is independent from @var{pattern}.
(define (copy-pattern pattern)
  (wrap-pattern
   (ffi:fontconfig-pattern-duplicate (unwrap-pattern pattern))))

;; Create a new pattern containing only those attributes of @var{pattern},
;; which are listed in @var{object-set}.
(define (pattern-filter pattern object-set)
  (wrap-pattern
   (ffi:fontconfig-pattern-filter
    (unwrap-pattern pattern) (unwrap-object-set object-set))))

(define (string->pattern str)
  (let ((ptr (ffi:fontconfig-name-parse (string->pointer str))))
    (if (null-pointer? ptr)
        (error "string->pattern failed")
        (wrap-pattern ptr))))

(define (pattern->string pattern)
  (let* ((ptr (ffi:fontconfig-name-unparse (unwrap-pattern pattern)))
         (str (if (null-pointer? ptr)
                  (scm-error 'memory-allocation-error
                             "pattern->string" "out of memory" '() #f)
                  (pointer->string ptr))))
    (ffi:fontconfig-str-free ptr)
    str))

;; https://www.freedesktop.org/software/fontconfig/fontconfig-devel/fcpatternformat.html
(define* (pattern->format pattern #:optional (format "%{=fclist}"))
  (let* ((ptr (ffi:fontconfig-pattern-format
               (unwrap-pattern pattern) (string->pointer format)))
         (formatted
          (if (null-pointer? ptr)
              (error "invalid format")
              (pointer->string ptr))))
    (ffi:fontconfig-str-free ptr)
    formatted))

;; Return the @var{id}'th value of @var{attribute} in pattern @var{pattern}.
(define* (pattern-get pattern attribute #:optional (id 0))
  (let-syntax ((match-result
                (syntax-rules ()
                  ((_ exp if-matched)
                   (match (ffi:int->fc-result exp)
                     ('match if-matched)
                     ('no-match *unspecified*)
                     ('no-id *unspecified*)
                     ('type-mismatch (error "type mismatch"))
                     ('out-of-memory (scm-error 'memory-allocation-error
                                                "pattern-get" "out of memory"
                                                '() #f)))))))
    (match (assoc attribute ffi:*attributes*)
      ((_ attr 'fc-bool)
       (let ((bv (make-bytevector (sizeof int))))
         (match-result
          (ffi:fontconfig-pattern-get-bool (unwrap-pattern pattern)
                                           (string->pointer attr)
                                           id (bytevector->pointer bv))
          (not (= (bytevector-s32-native-ref bv 0) 0)))))
      ((_ attr 'fc-char-set)
       (let* ((bv (make-bytevector (sizeof '*)))
              (ptr (bytevector->pointer bv)))
         (match-result
          (ffi:fontconfig-pattern-get-char-set (unwrap-pattern pattern)
                                               (string->pointer attr)
                                               id ptr)
          (let ((real-pointer (dereference-pointer ptr)))
            (and (not (null-pointer? real-pointer))
                 (fc-char-set->char-set (wrap-char-set real-pointer)))))))
      ((_ attr 'fc-integer)
       (let ((bv (make-bytevector (sizeof int))))
         (match-result
          (ffi:fontconfig-pattern-get-integer (unwrap-pattern pattern)
                                              (string->pointer attr)
                                              id (bytevector->pointer bv))
          (bytevector-s32-native-ref bv 0))))
      ((_ attr 'fc-double)
       (let ((bv (make-bytevector (sizeof double))))
         (match-result
          (ffi:fontconfig-pattern-get-double (unwrap-pattern pattern)
                                             (string->pointer attr)
                                             id (bytevector->pointer bv))
          (bytevector-ieee-double-native-ref bv 0))))
      ((_ attr 'fc-string)
       (let* ((bv (make-bytevector (sizeof '*)))
              (ptr (bytevector->pointer bv)))
         (match-result
          (ffi:fontconfig-pattern-get-string (unwrap-pattern pattern)
                                             (string->pointer attr)
                                             id ptr)
          (let ((real-pointer (dereference-pointer ptr)))
            (and (not (null-pointer? real-pointer))
                 (pointer->string real-pointer)))))))))

(define* (pattern-substitute! pattern #:key (config 'pattern) (default #t))
  (match (list config (and config (ffi:fc-match-kind->int config)))
    ((#f #f) *unspecified*)
    ((kind #f) (error "invalid match kind" kind))
    ((_ kind)
     (ffi:fontconfig-config-substitute (unwrap-config (*fontconfig*))
                                       (unwrap-pattern pattern)
                                       kind)))
  (when default
    (ffi:fontconfig-default-substitute (unwrap-pattern pattern)))
  pattern)

(define (pattern-substitute pattern . args)
  (apply pattern-substitute! (copy-pattern) args))

(define (font-match pattern)
  (let* ((%result (make-bytevector (sizeof int)))
         (%pat (ffi:fontconfig-font-match (unwrap-config (*fontconfig*))
                                          (unwrap-pattern pattern)
                                          (bytevector->pointer %result)))
         (result (ffi:int->fc-result (bytevector-s32-native-ref %result 0))))
    (match result
      ('match (wrap-pattern %pat))
      ('no-match #f)
      ('type-mismatch (error "type mismatch"))
      (_ (error "unexpected result" result)))))

(define (render-prepare pattern1 pattern2)
  (let ((%pat (ffi:fontconfig-font-render-prepare (unwrap-config (*fontconfig*))
                                                  (unwrap-pattern pattern1)
                                                  (unwrap-pattern pattern2))))
    (wrap-pattern %pat)))
