;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (fontconfig char-set)
             (quickcheck)
             (quickcheck arbitrary)
             (quickcheck property)
             (srfi srfi-64))

(test-begin "char-set")

(test-assert "char-set <=> fc-char-set"
  (quickcheck
   (property ((chars ($list $char))) ; XXX: refine into proper generator
     (let ((cs (list->char-set chars)))
       (peek cs)
       (char-set= cs
                  (fc-char-set->char-set
                   (char-set->fc-char-set cs)))))))

(test-end "char-set")
