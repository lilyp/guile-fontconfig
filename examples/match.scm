#!/bin/sh
exec guile -e main -s "$0" "$@"
!#

(use-modules (fontconfig)
             ((fontconfig debug) #:prefix debug:)
             (ice-9 getopt-long)
             (ice-9 match)
             (ice-9 optargs)
             (srfi srfi-26))

(define *grammar*
  '((version (single-char #\V) (value #f))
    (help    (single-char #\h) (value #f))
    (all     (single-char #\a) (value #f))
    (sort    (single-char #\s) (value #f))
    (brief   (single-char #\b) (value #f))
    (verbose (single-char #\v) (value #f))
    (format  (single-char #\f) (value #t))))

(define (do-match pattern os format all?)
  (let ((pattern (pattern-substitute pattern))
        (do-format
         (match format
           ('brief
            (lambda (pattern)
              ((@@ (fontconfig pattern) pattern-remove!) pattern "charset")
              ((@@ (fontconfig pattern) pattern-remove!) pattern "lang")
              (debug:print-pattern pattern)))
           ('verbose debug:print-pattern)
           (fmt (compose display (cute pattern->format <> fmt))))))
    (cond
     ((not (unspecified? all?))
      (for-each (compose do-format
                         (cute pattern-filter <> os)
                         (cute render-prepare pattern <>))
                (font-set->list (font-sort pattern all?))))
     (else (do-format
            (pattern-filter (font-match pattern) os))))))

(define (main args)
  (let* ((options (getopt-long args *grammar*))
         (option (cute option-ref options <> <>))
         (option? (cute option <> #f))
         (all?
          (cond
           ((option? 'all) #t)
           ((option? 'brief) #f)
           (else *unspecified*)))
         (default-object-set
           (cond
            ((option? 'brief) #f)
            ((option? 'verbose) #f)
            (else (object-set family style file))))
         (format
          (cond
           ((option? 'verbose) 'verbose)
           ((option? 'brief)   'brief)
           (else (option 'format
                         (match (option '() '())
                           (()  "%{=fcmatch}\n")
                           ((x) "%{=fcmatch}\n")
                           (_   "%{=unparse}\n")))))))
    (cond
     ((option? 'help)
      (display "\
usage: match.scm [OPTION...] [PATTERN [ELEMENT ...]]
List best font matching PATTERN

Options:
  -s, --sort           display sorted list of matches
  -a, --all            display unpruned sorted list of matches
  -v, --verbose        display entire font pattern verbosely
  -b, --brief          display entire font pattern briefly
  -f, --format=FORMAT  use the given output format
  -q, --quiet          suppress all normal output, exit 1 if no fonts matched
  -V, --version        display font config version and exit
  -h, --help           display this help and exit")
      (newline))
     ((option? 'version)
      (display "guile-fontconfig v0.1")
      (newline))
     (else
      (match (option '() '())
        (() (do-match (make-pattern) default-object-set format all?))
        ((pattern) (do-match (string->pattern pattern)
                             default-object-set
                             format
                             all?))
        ((pattern . elements)
         (do-match (string->pattern pattern)
                   (apply make-object-set (map string->symbol elements))
                   format
                   all?)))))))
