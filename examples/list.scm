#!/bin/sh
exec guile -e main -s "$0" "$@"
!#

(use-modules (fontconfig)
             ((fontconfig debug) #:prefix debug:)
             (ice-9 getopt-long)
             (ice-9 match)
             (ice-9 optargs)
             (srfi srfi-26))

(define *grammar*
  '((version (single-char #\V) (value #f))
    (help    (single-char #\h) (value #f))
    (brief   (single-char #\b) (value #f))
    (format  (single-char #\f) (value #t))
    (quiet   (single-char #\q) (value #f))
    (verbose (single-char #\v) (value #f))))

(define (do-list pattern os format)
  (let ((fs (font-list pattern os)))
    (match format
      ('quiet (exit (null? fs)))
      ('brief
       (for-each (lambda (pattern)
                   ((@@ (fontconfig pattern) pattern-remove!) pattern "charset")
                   ((@@ (fontconfig pattern) pattern-remove!) pattern "lang")
                   (debug:print-pattern pattern))
                 (font-set->list fs)))
      ('verbose
       (for-each debug:print-pattern (font-set->list fs)))
      (_
       (for-each
        (compose display (cute pattern->format <> format))
        (font-set->list fs))))))

(define (main args)
  (let* ((options (getopt-long args *grammar*))
         (option (cute option-ref options <> <>))
         (option? (cute option <> #f))
         (default-object-set
           (cond
            ((option? 'quiet) (make-object-set))
            ((option? 'brief) #f)
            ((option? 'verbose) #f)
            (else (object-set family style file))))
         (format
          (cond
           ((option? 'quiet)   'quiet)
           ((option? 'verbose) 'verbose)
           ((option? 'brief)   'brief)
           (else (option 'format "%{=fclist}\n")))))
    (cond
     ((option? 'help)
      (display "\
usage: list.scm [OPTION...] [PATTERN [ELEMENT ...]]
List fonts matching PATTERN

Options:
  -v, --verbose        display entire font pattern verbosely
  -b, --brief          display entire font pattern briefly
  -f, --format=FORMAT  use the given output format
  -q, --quiet          suppress all normal output, exit 1 if no fonts matched
  -V, --version        display font config version and exit
  -h, --help           display this help and exit")
      (newline))
     ((option? 'version)
      (display "guile-fontconfig v0.1")
      (newline))
     (else
      (match (option '() '())
        (() (do-list (make-pattern) default-object-set format))
        ((pattern) (do-list (string->pattern pattern)
                            default-object-set
                            format))
        ((pattern . elements)
         (do-list (string->pattern pattern)
                  (apply make-object-set (map string->symbol elements))
                  format)))))))

;; Local Variables:
;; mode: scheme
;; End:
